/**
 * 
 */
package com.demo.mapping;

import org.hibernate.cfg.AnnotationConfiguration;
import org.hibernate.tool.hbm2ddl.SchemaExport;

import com.demo.dao.Dao;
import com.demo.dao.DaoImpI;
import com.demo.entity.Dept;
import com.demo.entity.Education;
import com.demo.entity.Employee;
import com.demo.entity.Limits;
import com.demo.entity.Rolers;
import com.demo.entity.Users;

/**
 * @author Administrator
 * 
 */

public class Test {
	public static void main(String[] args) {
		new SchemaExport(new AnnotationConfiguration().configure()).create(
				true, true);
		
		Dao dao=new DaoImpI();
		
		Education education1=new Education();
		education1.setEduName("本科");
		dao.saveObject(education1);
		Education education2=new Education();
		education2.setEduName("硕士");
		dao.saveObject(education2);
		Education education3=new Education();
		education3.setEduName("博士");
		dao.saveObject(education3);
		Education education4=new Education();
		education4.setEduName("高职高专");
		dao.saveObject(education4);
		
		//添加权限
		Limits limits1=new Limits();
		limits1.setLimitName("管理员");
		dao.saveObject(limits1);
		
		Limits limits2=new Limits();
		limits2.setLimitName("普通员工");
		dao.saveObject(limits2);
		
		//添加部门
		Dept dept1=new Dept();
		dept1.setDeptName("研发部");
		dept1.setCreateTime("2005-09-01");
		dao.saveObject(dept1);
		
		Dept dept2=new Dept();
		dept2.setDeptName("科技部");
		dept2.setCreateTime("2005-10-01");
		dao.saveObject(dept2);
		
		//添加角色
		Rolers rolers1=new Rolers();
		rolers1.setRolerName("Boss");
		rolers1.setLimits(limits2);
		dao.saveObject(rolers1);
		
		Rolers rolers2=new Rolers();
		rolers2.setRolerName("Manager");
		rolers2.setLimits(limits2);
		dao.saveObject(rolers2);
		
		Rolers rolers3=new Rolers();
		rolers3.setRolerName("Employee");
		rolers3.setLimits(limits2);
		dao.saveObject(rolers3);
		
		//添加管理员数据，普通用户数据
		    //管理员
				Users users1=new Users();
				users1.setUserName("admin");
				users1.setUserPassword("admin");
				users1.setLimits(limits1);
				dao.saveObject(users1);
			//普通用户1	
				Users users2=new Users();
				users2.setUserName("123456");
				users2.setUserPassword("123456");
				users2.setLimits(limits2);
				dao.saveObject(users2);
			//普通用户2	
				Users users3=new Users();
				users3.setUserName("xiaopp");
				users3.setUserPassword("xiaopp");
				users3.setLimits(limits2);
				dao.saveObject(users3);
			
		//添加员工
				Employee employee1=new Employee();
				employee1.setSex("1");
				employee1.setBirthDay("1980-08-26");
				employee1.setEducation(education1);
				employee1.setEmpName(users2.getUserName());
				employee1.setDept(dept1);
				employee1.setRolers(rolers3);
				dao.saveObject(employee1);
				
				Employee employee2=new Employee();
				employee2.setSex("0");
				employee2.setBirthDay("1987-09-21");
				employee2.setDept(dept2);
				employee2.setEducation(education2);
				employee2.setEmpName(users3.getUserName());
				employee2.setRolers(rolers2);
				dao.saveObject(employee2);
				
	}
}
