/**
 * 
 */
package com.demo.service;

import java.util.List;

import com.demo.entity.Dept;

/**
 * @author Administrator
 * 
 */
public interface DeptService {

	// 分页查询
	List<Dept> findWithPage(int page, int rows);

	// 查询总行数
	int getRows();
	//保存数据
	void saveDeptInfo(Dept dept);
	//
	Dept findByIdIfo(int id);
	//更新数据
	void updateDeptInfo(Dept dept);
	//删除数据
	void deleteDeptInfo(Dept dept);
	//查询所有


	List<Dept> findAllInfo();

	Dept findByName(String Name);

	List<Dept> findAllDeptInfo();
}
