/**
 * 
 */
package com.demo.service;

import java.util.List;

import com.demo.entity.Education;

/**
 * @author Administrator
 *
 */
public interface EduService {

	List<Education> findAllInfo();

	Education findByIdIfo(int id);
}
