/**
 * 
 */
package com.demo.service;

import java.util.List;

import com.demo.entity.Rolers;

/**
 * @author Administrator
 *
 */
public interface RolerService {

	// 分页查询
	List<Rolers> findWithPage(int page, int rows);

	// 查询总行数
	int getRows();
	//保存数据
	void saveRolerInfo(Rolers rolers);
	//
	Rolers findByIdIfo(int id);
	//更新数据
	void updateRolerInfo(Rolers rolers);
	//删除数据
	void deleteRolerInfo(Rolers rolers);

	List<Rolers> findAllInfo();

	List<Rolers> findAllRolerInfo();
}
