/**
 * 
 */
package com.demo.service;

import java.util.List;

import com.demo.dao.Dao;
import com.demo.dao.DaoImpI;
import com.demo.entity.Dept;

/**
 * @author Administrator
 * 
 */
@SuppressWarnings("unchecked")
public class DeptServiceImpI implements DeptService {

	private Dao dao = new DaoImpI();

	/*
	 * 
	 * @see com.demo.service.DeptService#findWithPage(int, int)
	 */

	@Override
	public List<Dept> findWithPage(int page, int rows) {
		// Auto-generated method stub
		return (List<Dept>) dao.findWithPage(page, rows, "from Dept");
	}

	/*
	 * 
	 * @see com.demo.service.DeptService#getRows()
	 */
	@Override
	public int getRows() {
		Object obj = dao.getObject("select count(*) from Dept");
		long temObj = (Long) obj;
		return (int) temObj;
	}

	@Override
	public void saveDeptInfo(Dept dept) {

		dao.saveObject(dept);
	}

	@Override
	public Dept findByIdIfo(int id) {

		return (Dept) dao.getObject("from Dept where deptId=" + id);
	}

	@Override
	public void updateDeptInfo(Dept dept) {
		dao.updateObject(dept);

	}

	@Override
	public void deleteDeptInfo(Dept dept) {
		dao.deleteObject(dept);
		
	}

	@SuppressWarnings("rawtypes")
	@Override
	public List findAllInfo() {
		return (List<Dept>) dao.findAllInfo("from Dept");
	}

	@Override
	public Dept findByName(String Name) {
		return (Dept) dao.getObject("from Dept where deptName=" + Name);
	}

	@Override
	public List<Dept> findAllDeptInfo() {
		return  (List<Dept>) dao.findAllInfo("from Dept");
	}

}
