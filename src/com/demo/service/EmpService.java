/**
 * 
 */
package com.demo.service;

import java.util.List;

import com.demo.entity.Employee;


/**
 * @author Administrator
 *
 */
public interface EmpService {

	List<Employee>findWithPage(int page,int rows);
	int getRows();
	//保存数据
	void saveEmpInfo(Employee employee);
	//
	Employee findByIdIfo(int id);
	//更新数据
	void updateEmpInfo(Employee employee);
	//删除数据
	void deleteEmpInfo(Employee employee);
	//查询所有


	List<Employee> findAllInfo();
	
	List<Employee> findAllEmpInfo(String empName);
}
