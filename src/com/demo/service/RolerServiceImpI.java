/**
 * 
 */
package com.demo.service;

import java.util.List;

import com.demo.dao.Dao;
import com.demo.dao.DaoImpI;
import com.demo.entity.Rolers;

/**
 * @author Administrator
 *
 */
@SuppressWarnings("unchecked")
public class RolerServiceImpI implements RolerService {

	private Dao dao = new DaoImpI();

	@Override
	public List<Rolers> findWithPage(int page, int rows) {
		return (List<Rolers>) dao.findWithPage(page, rows, "from Rolers");
	}

	@Override
	public int getRows() {
		Object obj = dao.getObject("select count(*) from Rolers");
		long temObj = (Long) obj;
		return (int) temObj;
	}

	@Override
	public void saveRolerInfo(Rolers rolers) {

		dao.saveObject(rolers);
	}

	@Override
	public Rolers findByIdIfo(int id) {

		return (Rolers) dao.getObject("from Rolers where rolerId=" + id);
	}

	@Override
	public void updateRolerInfo(Rolers rolers) {
		dao.updateObject(rolers);

	}

	@Override
	public void deleteRolerInfo(Rolers rolers) {
		dao.deleteObject(rolers);
		
	}

	@SuppressWarnings("rawtypes")
	@Override
	public List findAllInfo() {
		// TODO Auto-generated method stub
		return (List<Rolers>) dao.findAllInfo("from Rolers");
	}

	@Override
	public List<Rolers> findAllRolerInfo() {
		// TODO Auto-generated method stub
		return (List<Rolers>) dao.findAllInfo("from Rolers");
	}


}
