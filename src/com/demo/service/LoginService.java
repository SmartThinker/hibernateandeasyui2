/**
 * 
 */
package com.demo.service;

import com.demo.entity.Users;


/**
 * @author Administrator
 *
 */
public interface LoginService {

	boolean checkFindByUserName(String userName);

	Users isPass(String userName, String userPassword);

}
