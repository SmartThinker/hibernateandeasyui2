/**
 * 
 */
package com.demo.service;

import java.util.List;

import com.demo.dao.Dao;
import com.demo.dao.DaoImpI;
import com.demo.entity.Employee;

/**
 * @author Administrator
 * 
 */
public class EmpServiceImpI implements EmpService {

	private Dao dao = new DaoImpI();

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.demo.service.EmpService#findWithPage(int, int, java.lang.String)
	 */
	@SuppressWarnings("unchecked")
	@Override
	public List<Employee> findWithPage(int page, int rows) {
		return (List<Employee>) dao.findWithPage(page, rows, "from Employee");
	}

	@Override
	public int getRows() {
		Object object=dao.getObject("select count(*) from Employee");
		long temp=(Long) object;
		return (int) temp;
	}

	@Override
	public void saveEmpInfo(Employee employee) {
		dao.saveObject(employee);
		
	}

	@Override
	public Employee findByIdIfo(int id) {
		return (Employee) dao.getObject("from Employee where empId="+id);
	}

	@Override
	public void updateEmpInfo(Employee employee) {
		dao.updateObject(employee);
		
	}

	@Override
	public void deleteEmpInfo(Employee employee) {
		dao.deleteObject(employee);
		
	}

	@SuppressWarnings("unchecked" )
	@Override
	public List<Employee> findAllInfo() {
		// TODO Auto-generated method stub
		return (List<Employee>) dao.findAllInfo("from Employee");
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Employee> findAllEmpInfo(String empName) {
		// TODO Auto-generated method stub
		return (List<Employee>) dao.findAllInfo("from Employee where empName='"+empName+"'");
	}

}
