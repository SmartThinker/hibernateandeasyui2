/**
 * 
 */
package com.demo.service;

import java.util.List;

import com.demo.dao.Dao;
import com.demo.dao.DaoImpI;
import com.demo.entity.Education;

/**
 * @author Administrator
 * 
 */
public class EduServiceImpI implements EduService {
	private Dao dao = new DaoImpI();

	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Override
	public List findAllInfo() {
		return (List<Education>) dao.findAllInfo("from Education");
	}

	@Override
	public Education findByIdIfo(int id) {
		// TODO Auto-generated method stub
		return (Education) dao.getObject("from Education where educationId=" + id);
	}

}
