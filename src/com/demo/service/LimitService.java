/**
 * 
 */
package com.demo.service;

import java.util.List;

import com.demo.entity.Limits;

/**
 * @author Administrator
 *
 */
public interface LimitService {

	List<Limits> findWithPage(int page, int rows);

	int getRows();

	void saveLimitInfo(Limits limits);

	Limits findByIdIfo(int id);

	void updateLimitInfo(Limits limits);

	void deleteLimitInfo(Limits limits);

	List<Limits> findAllInfo();

	List<Limits> findAllEmpInfo(String limitName);

}
