/**
 * 
 */
package com.demo.service;

import java.util.List;

import com.demo.dao.Dao;
import com.demo.dao.DaoImpI;
import com.demo.entity.Limits;

/**
 * @author Administrator
 *
 */
public class LimitServiceImpI implements LimitService {
	private Dao dao = new DaoImpI();
	@SuppressWarnings("unchecked")
	@Override
	public List<Limits> findWithPage(int page, int rows) {
		// TODO Auto-generated method stub
		return (List<Limits>) dao.findWithPage(page, rows, "from Limits");
	}

	@Override
	public int getRows() {
		Object obj = dao.getObject("select count(*) from Limits");
		long temObj = (Long) obj;
		return (int) temObj;
	}

	@Override
	public void saveLimitInfo(Limits limits) {
		// TODO Auto-generated method stub
		dao.saveObject(limits);
	}

	@Override
	public Limits findByIdIfo(int id) {
		return (Limits) dao.getObject("from Limits where limitId=" + id);
	}

	@Override
	public void updateLimitInfo(Limits limits) {
		// TODO Auto-generated method stub
		dao.updateObject(limits);
	}

	@Override
	public void deleteLimitInfo(Limits limits) {
		// TODO Auto-generated method stub
		dao.deleteObject(limits);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Limits> findAllInfo() {
		return (List<Limits>) dao.findAllInfo("from Limits");
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Limits> findAllEmpInfo(String limitName) {
		// TODO Auto-generated method stub
		return (List<Limits>) dao.findAllInfo("from Limits where limitName='"+limitName+"'");
	}

	
	
}
