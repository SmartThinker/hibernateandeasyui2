/**
 * 
 */
package com.demo.utils;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.AnnotationConfiguration;

/**
 * @author Administrator
 * 
 */
public class HibernateUtils {

	private static SessionFactory sessionFactory = null;
	static {
		try {
			sessionFactory = new AnnotationConfiguration().configure()
					.buildSessionFactory();
		} catch (Exception e) {
			e.getStackTrace();
		}
	}

	public static Session getSession() {
		return sessionFactory.openSession();
	}

	public static void getClosed(Session session) {
		if (session != null) {
			session.close();
		}
	}
}
