/**
 * 
 */
package com.demo.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * @author Administrator
 * 
 */
@Entity
@Table(name = "tbRolers")
public class Rolers {

	private Integer rolerId;
	private String rolerName;
	private String rolerDesc;
	private String rolerRemark;

	private Limits limits;

	@ManyToOne
	@JoinColumn(name = "fk_r_l")
	public Limits getLimits() {
		return limits;
	}

	public void setLimits(Limits limits) {
		this.limits = limits;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	public Integer getRolerId() {
		return rolerId;
	}

	public void setRolerId(Integer rolerId) {
		this.rolerId = rolerId;
	}

	public String getRolerName() {
		return rolerName;
	}

	public void setRolerName(String rolerName) {
		this.rolerName = rolerName;
	}

	public String getRolerDesc() {
		return rolerDesc;
	}

	public void setRolerDesc(String rolerDesc) {
		this.rolerDesc = rolerDesc;
	}

	public String getRolerRemark() {
		return rolerRemark;
	}

	public void setRolerRemark(String rolerRemark) {
		this.rolerRemark = rolerRemark;
	}

	@Override
	public String toString() {
		return "Rolers [rolerId=" + rolerId + ", rolerName=" + rolerName
				+ ", rolerDesc=" + rolerDesc + ", rolerRemark=" + rolerRemark
				+ ", limits=" + limits + "]";
	}

	
	
}
