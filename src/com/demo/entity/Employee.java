/**
 * 
 */
package com.demo.entity;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * @author Administrator
 * 
 */
@Entity
@Table(name = "tbEmployee")
public class Employee {

	private Integer empId;
	private String empName;
	private String sex;
	private String birthDay;
	private String empDesc;
	private String empRemark;
	private String deptTemp;
	private String eduTemp;
	private String rolerTemp;
	

	public String getDeptTemp() {
		return deptTemp;
	}

	public void setDeptTemp(String deptTemp) {
		this.deptTemp = deptTemp;
	}

	public String getEduTemp() {
		return eduTemp;
	}

	public void setEduTemp(String eduTemp) {
		this.eduTemp = eduTemp;
	}

	public String getRolerTemp() {
		return rolerTemp;
	}

	public void setRolerTemp(String rolerTemp) {
		this.rolerTemp = rolerTemp;
	}

	private Dept dept;

	private Education education;

	private Rolers rolers;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "fk_r_e")
	public Rolers getRolers() {
		return rolers;
	}

	public void setRolers(Rolers rolers) {
		this.rolers = rolers;
	}

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "fk_e_e")
	public Education getEducation() {
		return education;
	}

	public void setEducation(Education education) {
		this.education = education;
	}

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "fk_e_d")
	public Dept getDept() {
		return dept;
	}

	public void setDept(Dept dept) {
		this.dept = dept;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	public Integer getEmpId() {
		return empId;
	}

	public void setEmpId(Integer empId) {
		this.empId = empId;
	}

	public String getEmpName() {
		return empName;
	}

	public void setEmpName(String empName) {
		this.empName = empName;
	}

	public String getSex() {
		return sex;
	}

	public void setSex(String sex) {
		this.sex = sex;
	}

	public String getBirthDay() {
		return birthDay;
	}

	public void setBirthDay(String birthDay) {
		this.birthDay = birthDay;
	}

	public String getEmpDesc() {
		return empDesc;
	}

	public void setEmpDesc(String empDesc) {
		this.empDesc = empDesc;
	}

	public String getEmpRemark() {
		return empRemark;
	}

	public void setEmpRemark(String empRemark) {
		this.empRemark = empRemark;
	}

}
