/**
 * 
 */
package com.demo.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * @author Administrator
 * 
 */
@Entity
@Table(name = "tbEducation")
public class Education {

	private Integer educationId;
	private String eduName;
	private String eduRemark;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	public Integer getEducationId() {
		return educationId;
	}

	public void setEducationId(Integer educationId) {
		this.educationId = educationId;
	}

	public String getEduName() {
		return eduName;
	}

	public void setEduName(String eduName) {
		this.eduName = eduName;
	}

	public String getEduRemark() {
		return eduRemark;
	}

	public void setEduRemark(String eduRemark) {
		this.eduRemark = eduRemark;
	}

}
