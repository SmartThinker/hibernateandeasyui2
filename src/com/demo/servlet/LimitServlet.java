/**
 * 
 */
package com.demo.servlet;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.demo.entity.Limits;
import com.demo.service.LimitService;
import com.demo.service.LimitServiceImpI;
import com.google.gson.GsonBuilder;

/**
 * @author Administrator
 *
 */
@SuppressWarnings("serial")
public class LimitServlet extends HttpServlet {

	private LimitService limitService=new LimitServiceImpI();
	Limits limits=new Limits();
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		this.doPost(req, resp);
	}
	@SuppressWarnings("rawtypes")
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		resp.setCharacterEncoding("utf-8");
		req.setCharacterEncoding("utf-8");
		String method = req.getParameter("m");
		if ("findWithPage".equals(method)) {
			int page = 0;
			int rows = 0;
			if (!"".equals(req.getParameter("page"))
					&& req.getParameter("page") != null) {
				page = Integer.parseInt(req.getParameter("page"));
			}
			if (!"".equals(req.getParameter("rows"))
					&& req.getParameter("rows") != null) {
				rows = Integer.parseInt(req.getParameter("rows"));
			}
			List list = limitService.findWithPage(page, rows);
			Map<String, Object> map = new HashMap<String, Object>();
			map.put("total", limitService.getRows());
			map.put("rows", list);
			resp.getWriter().print(new GsonBuilder().create().toJson(map));
			resp.getWriter().flush();
			resp.getWriter().close();
		} else if ("saveLimitInfo".equals(method)) {
			limits.setLimitName(req.getParameter("limitName"));
			limits.setLimitDesc(req.getParameter("limitDesc"));
			limits.setLimitRemark(req.getParameter("limitRemark"));
			limitService.saveLimitInfo(limits);
		} 
		else if ("findByIdInfo".equals(method)) {
			limits = limitService.findByIdIfo(Integer.parseInt(req
					.getParameter("f_id")));
			resp.getWriter().print(new GsonBuilder().create().toJson(limits));
			resp.getWriter().flush();
			resp.getWriter().close();
		}else if ("updateLimitInfo".equals(method)) {
			limits.setLimitName(req.getParameter("limitName"));
			limits.setLimitDesc(req.getParameter("limitDesc"));
			limits.setLimitRemark(req.getParameter("limitRemark"));
			limitService.updateLimitInfo(limits);
		}else if ("deleteLimitInfo".equals(method)) {
			limits.setLimitId(Integer.parseInt(req.getParameter("d_id")));
			limitService.deleteLimitInfo(limits);
		}else if ("findAllInfo".equals(method)) {
			List<Limits> list=limitService.findAllInfo();
			resp.getWriter().print(new GsonBuilder().create().toJson(list));
			resp.getWriter().flush();
			resp.getWriter().close();
		}else if ("saveCheckEmpNameInfo".equals(method)) {
			boolean flag = false;
			String limitName = req.getParameter("limitName");
			List<Limits> list = limitService.findAllEmpInfo(limitName);
			for (Limits limits : list) {
				//System.out.println(limits.getLimitName().equals(limitName));
				if (limits.getLimitName().equals(limitName)==true) {
					flag = true;
					break;
				} else {
					flag = false;
					break;
				}
			}
			resp.getWriter().print(flag);
			resp.getWriter().flush();
			resp.getWriter().close();
		}
	}
}
