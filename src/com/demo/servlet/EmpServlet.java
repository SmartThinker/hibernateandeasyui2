/**
 * 
 */
package com.demo.servlet;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.demo.entity.Dept;
import com.demo.entity.Education;
import com.demo.entity.Employee;
import com.demo.entity.Rolers;
import com.demo.service.DeptService;
import com.demo.service.DeptServiceImpI;
import com.demo.service.EduService;
import com.demo.service.EduServiceImpI;
import com.demo.service.EmpService;
import com.demo.service.EmpServiceImpI;
import com.demo.service.RolerService;
import com.demo.service.RolerServiceImpI;
import com.google.gson.GsonBuilder;

/**
 * @author Administrator
 * 
 */
@SuppressWarnings("serial")
public class EmpServlet extends HttpServlet {
	private EmpService empService = new EmpServiceImpI();
	private DeptService deptService = new DeptServiceImpI();
	private RolerService rolerService = new RolerServiceImpI();
	private EduService eduService = new EduServiceImpI();
	Employee employee = new Employee();

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		this.doPost(req, resp);
	}

	@SuppressWarnings("rawtypes")
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		resp.setCharacterEncoding("utf-8");
		req.setCharacterEncoding("utf-8");
		String method = req.getParameter("m");
		if ("findWithPage".equals(method)) {
			int page = 0;
			int rows = 0;
			if (!"".equals(req.getParameter("page"))
					&& req.getParameter("page") != null) {
				page = Integer.parseInt(req.getParameter("page"));
			}
			if (!"".equals(req.getParameter("rows"))
					&& req.getParameter("rows") != null) {
				rows = Integer.parseInt(req.getParameter("rows"));
			}
			List<Employee> list = empService.findWithPage(page, rows);
			for (Employee e : list) {
				if ("0".equals(e.getSex())) {
					e.setSex("男");

				} else {
					e.setSex("女");
				}

			}
			Map<String, Object> map = new HashMap<String, Object>();
			map.put("total", empService.getRows());
			map.put("rows", list);
			resp.getWriter().print(new GsonBuilder().create().toJson(map));
			resp.getWriter().flush();
			resp.getWriter().close();
		} else if ("saveEmpInfo".equals(method)) {
			Dept dept = deptService.findByIdIfo(Integer.parseInt(req
					.getParameter("deptId")));
			Education education = eduService.findByIdIfo(Integer.parseInt(req
					.getParameter("educationId")));
			Rolers rolers = rolerService.findByIdIfo(Integer.parseInt(req
					.getParameter("rolerId")));
			employee.setEmpName(req.getParameter("empName"));
			employee.setSex(req.getParameter("sex"));
			employee.setBirthDay(req.getParameter("birthDay"));
			employee.setEmpDesc(req.getParameter("empDesc"));
			employee.setEmpRemark(req.getParameter("empRemark"));
			employee.setDept(dept);
			employee.setEducation(education);
			employee.setRolers(rolers);
			empService.saveEmpInfo(employee);
		} else if ("findByIdIfo".equals(method)) {
			employee = empService.findByIdIfo(Integer.parseInt(req
					.getParameter("f_id")));
			resp.getWriter().print(new GsonBuilder().create().toJson(employee));
			resp.getWriter().flush();
			resp.getWriter().close();
		} else if ("updateEmpInfo".equals(method)) {
			employee.setEmpName(req.getParameter("empName"));
			employee.setSex(req.getParameter("sex"));
			employee.setBirthDay(req.getParameter("birthDay"));
			employee.setEmpDesc(req.getParameter("empDesc"));
			employee.setEmpRemark(req.getParameter("empRemark"));
			// 部门
			Dept dept = new Dept();
			Education education = new Education();
			Rolers rolers = new Rolers();
			boolean flag1 = true;
			List<Dept> list1 = deptService.findAllInfo();
			for (Dept d : list1) {
				if (d.getDeptName().equals(req.getParameter("deptId"))) {
					flag1 = false;
				}
			}
			if (!flag1) {
				employee.setDeptTemp(req.getParameter("deptId"));
			} else {
				dept = deptService.findByIdIfo(Integer.parseInt(req
						.getParameter("deptId")));
				employee.setDept(dept);
			}
			// 学历
			boolean flag2 = true;
			List<Education> list2 = eduService.findAllInfo();
			for (Education e : list2) {
				System.out.println(e.getEducationId());
				System.out.println(req.getParameter("educationId"));
				if (e.getEduName().equals(req.getParameter("educationId"))) {
					flag2 = false;
				}
			}
			if (!flag2) {
				employee.setEduTemp(req.getParameter("eduId"));
			} else {
				education = eduService.findByIdIfo(Integer.parseInt(req
						.getParameter("eduId")));
				employee.setEducation(education);
			}
			// 角色
			boolean flag3 = true;
			List<Rolers> list3 = rolerService.findAllInfo();
			for (Rolers r : list3) {
				System.out.println(r.getRolerId());
				System.out.println(req.getParameter("rolerId"));
				if (r.getRolerName().equals(req.getParameter("rolerId"))) {
					flag3 = false;
				}
			}
			if (!flag3) {
				employee.setRolerTemp(req.getParameter("rolerId"));
			} else {
				rolers = rolerService.findByIdIfo(Integer.parseInt(req
						.getParameter("rolerId")));
				employee.setRolers(rolers);
			}

			empService.updateEmpInfo(employee);
		} else if ("deleteEmpInfo".equals(method)) {
			employee.setEmpId(Integer.parseInt(req.getParameter("d_id")));
			empService.deleteEmpInfo(employee);
		} else if ("findAllInfo".equals(method)) {
			List list = empService.findAllInfo();
			resp.getWriter().print(new GsonBuilder().create().toJson(list));
			resp.getWriter().flush();
			resp.getWriter().close();
		} else if ("saveCheckEmpNameInfo".equals(method)) {
			boolean flag = false;
			String empName = req.getParameter("empName");
			List<Employee> list = empService.findAllEmpInfo(empName);
			for (Employee employee : list) {
				System.out.println(employee.getEmpName().equals(empName));
				if (employee.getEmpName().equals(empName) == true) {
					flag = true;
					break;
				} else {
					flag = false;
					break;
				}
			}
			resp.getWriter().print(flag);
			resp.getWriter().flush();
			resp.getWriter().close();
		}
	}
}
