/**
 * 
 */
package com.demo.servlet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.demo.entity.Users;
import com.demo.service.LoginService;
import com.demo.service.LoginServiceImpI;
import com.google.gson.GsonBuilder;

/**
 * @author Administrator
 * 
 */
public class LoginServlet extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3167328696147534849L;
	private LoginService loginService = new LoginServiceImpI();
	Users users = new Users();

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		this.doPost(req, resp);
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {

		req.setCharacterEncoding("utf-8");
		resp.setCharacterEncoding("utf-8");
		String method = req.getParameter("m");
		if ("".equals(method)) {
			req.getRequestDispatcher("./login.jsp").forward(req, resp);
		} else {
			if ("checkUserAccess".equals(method)) {
				String userName = req.getParameter("userName");
				String userPassword = req.getParameter("userPassword");
			//	int limitId=Integer.parseInt(req.getParameter("limitId"));
			//	String limitId=req.getParameter("limitId");
				boolean flagCheckUser=loginService.checkFindByUserName(userName);
				if (flagCheckUser==false) {
					//用户存在1
					//检测密码是否匹配
					Users users=null;
					users=loginService.isPass(userName,userPassword);
					if (users!=null) {
						//密码正确2
						//检测权限
						if ("管理员".equals(users.getLimits().getLimitName())) {
							//管理员1
							req.getSession().setAttribute("userType", 1);
						}else{
							//普通用户0
							req.getSession().setAttribute("userType", 0);
						}
						resp.getWriter().print(new GsonBuilder().create().toJson(2));
						resp.getWriter().flush();
						resp.getWriter().close();
					}else{
						//密码不正确3
						resp.getWriter().print(new GsonBuilder().create().toJson(3));
						resp.getWriter().flush();
						resp.getWriter().close();
					}
				}else{
					//用户不存在0
					resp.getWriter().print(new GsonBuilder().create().toJson(0));
					resp.getWriter().flush();
					resp.getWriter().close();
				}
			}
			
			
		}
	}
}
