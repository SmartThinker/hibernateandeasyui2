/**
 * 
 */
package com.demo.servlet;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.demo.service.EduService;
import com.demo.service.EduServiceImpI;
import com.google.gson.GsonBuilder;

/**
 * @author Administrator
 *
 */
@SuppressWarnings("serial")
public class EduServlet extends HttpServlet {

	private EduService eduService=new EduServiceImpI();
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		this.doPost(req, resp);
	}
	@SuppressWarnings("rawtypes")
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		resp.setCharacterEncoding("utf-8");
		req.setCharacterEncoding("utf-8");
		String method = req.getParameter("m");
		if ("findAllEduInfo".equals(method)) {
			List list=eduService.findAllInfo();
			resp.getWriter().print(new GsonBuilder().create().toJson(list));
			resp.getWriter().flush();
			resp.getWriter().close();
			
		}
	}
}
