/**
 * 
 */
package com.demo.servlet;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.demo.entity.Dept;
import com.demo.service.DeptService;
import com.demo.service.DeptServiceImpI;
import com.google.gson.GsonBuilder;

/**
 * @author Administrator
 * 
 */
@SuppressWarnings("rawtypes")
public class DeptServlet extends HttpServlet {

	/**
	 * 
	 */

	private static final long serialVersionUID = 1L;

	private DeptService deptService = new DeptServiceImpI();

	Dept dept = new Dept();

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		this.doPost(req, resp);
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		resp.setCharacterEncoding("utf-8");
		req.setCharacterEncoding("utf-8");
		String method = req.getParameter("m");
		if ("findWithPage".equals(method)) {
			int page = 0;
			int rows = 0;
			if (!"".equals(req.getParameter("page"))
					&& req.getParameter("page") != null) {
				page = Integer.parseInt(req.getParameter("page"));
			}
			if (!"".equals(req.getParameter("rows"))
					&& req.getParameter("rows") != null) {
				rows = Integer.parseInt(req.getParameter("rows"));
			}
			List list = deptService.findWithPage(page, rows);
			Map<String, Object> map = new HashMap<String, Object>();
			map.put("total", deptService.getRows());
			map.put("rows", list);
			resp.getWriter().print(new GsonBuilder().create().toJson(map));
			resp.getWriter().flush();
			resp.getWriter().close();
		}
		else if ("saveCheckDeptNameInfo".equals(method)) {
			boolean flag = false;
			String deptName = req.getParameter("deptName");
			List<Dept> list = deptService.findAllDeptInfo();
			for (Dept dept : list) {
				System.out.println(dept.getDeptName().equals(deptName));
				if (dept.getDeptName().equals(deptName)==true) {
					flag = true;
					break;
				} else {
					flag = false;
					break;
				}
			}
			resp.getWriter().print(flag);
			resp.getWriter().flush();
			resp.getWriter().close();
		} 
		else if ("saveDeptInfo".equals(method)) {
			dept.setDeptName(req.getParameter("deptName"));
			dept.setCreateTime(req.getParameter("createTime"));
			dept.setModifyTime(req.getParameter("modifyTime"));
			dept.setDeptDesc(req.getParameter("deptDesc"));
			dept.setEmpRemark(req.getParameter("deptRemark"));
			deptService.saveDeptInfo(dept);
		} else if ("findByIdIfo".equals(method)) {
			dept = deptService.findByIdIfo(Integer.parseInt(req
					.getParameter("f_id")));
			resp.getWriter().print(new GsonBuilder().create().toJson(dept));
			resp.getWriter().flush();
			resp.getWriter().close();
		}else if ("updateDeptInfo".equals(method)) {
			dept.setDeptName(req.getParameter("deptName"));
			dept.setCreateTime(req.getParameter("createTime"));
			dept.setModifyTime(req.getParameter("modifyTime"));
			dept.setDeptDesc(req.getParameter("deptDesc"));
			dept.setEmpRemark(req.getParameter("empRemark"));
			deptService.updateDeptInfo(dept);
		}else if ("deleteDeptInfo".equals(method)) {
			dept.setDeptId(Integer.parseInt(req.getParameter("d_id")));
			deptService.deleteDeptInfo(dept);
		}else if ("findAllInfo".equals(method)) {
			List list=deptService.findAllInfo();
			resp.getWriter().print(new GsonBuilder().create().toJson(list));
			resp.getWriter().flush();
			resp.getWriter().close();
		}
	}
}
