/**
 * 
 */
package com.demo.servlet;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.demo.entity.Limits;
import com.demo.entity.Rolers;
import com.demo.service.LimitService;
import com.demo.service.LimitServiceImpI;
import com.demo.service.RolerService;
import com.demo.service.RolerServiceImpI;
import com.google.gson.GsonBuilder;

/**
 * @author Administrator
 * 
 */
@SuppressWarnings("serial")
public class RolerServlet extends HttpServlet {

	private RolerService rolerService = new RolerServiceImpI();
	private LimitService limitService = new LimitServiceImpI();
	Limits limits = new Limits();
	Rolers rolers = new Rolers();

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		this.doPost(req, resp);
	}

	@SuppressWarnings("rawtypes")
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		resp.setCharacterEncoding("utf-8");
		req.setCharacterEncoding("utf-8");
		String method = req.getParameter("m");
		if ("findWithPage".equals(method)) {
			int page = 0;
			int rows = 0;
			if (!"".equals(req.getParameter("page"))
					&& req.getParameter("page") != null) {
				page = Integer.parseInt(req.getParameter("page"));
			}
			if (!"".equals(req.getParameter("rows"))
					&& req.getParameter("rows") != null) {
				rows = Integer.parseInt(req.getParameter("rows"));
			}

			List list = rolerService.findWithPage(page, rows);
			Map<String, Object> map = new HashMap<String, Object>();
			map.put("total", rolerService.getRows());
			map.put("rows", list);
			resp.getWriter().print(new GsonBuilder().create().toJson(map));
			resp.getWriter().flush();
			resp.getWriter().close();
		} else if ("saveRolerInfo".equals(method)) {
			limits = limitService.findByIdIfo(Integer.parseInt(req
					.getParameter("limitId")));
			rolers.setRolerName(req.getParameter("rolerName"));
			rolers.setRolerDesc(req.getParameter("rolerDesc"));
			rolers.setRolerRemark(req.getParameter("rolerRemark"));
			rolers.setLimits(limits);
			rolerService.saveRolerInfo(rolers);
		} else if ("findByIdIfo".equals(method)) {
			rolers = rolerService.findByIdIfo(Integer.parseInt(req
					.getParameter("f_id")));
			resp.getWriter().print(new GsonBuilder().create().toJson(rolers));
			resp.getWriter().flush();
			resp.getWriter().close();
		} else if ("updateRolerInfo".equals(method)) {
			limits = limitService.findByIdIfo(Integer.parseInt(req
					.getParameter("limitId")));
//			boolean flag=false;
//			List<Limits> list=limitService.findAllInfo();
//			for (Limits l : list) {
//				if (l.getLimitName().equals(req.getParameter("limitId"))) {
//					flag=true;
//				}
//			}
			rolers.setRolerName(req.getParameter("rolerName"));
			rolers.setRolerDesc(req.getParameter("rolerDesc"));
			rolers.setRolerRemark(req.getParameter("rolerRemark"));
			rolers.setLimits(limits);
			rolerService.updateRolerInfo(rolers);
		} else if ("deleteRolerInfo".equals(method)) {
			rolers.setRolerId(Integer.parseInt(req.getParameter("d_id")));
			rolerService.deleteRolerInfo(rolers);
		} else if ("findAllRolerInfo".equals(method)) {
			List list = rolerService.findAllInfo();
			resp.getWriter().print(new GsonBuilder().create().toJson(list));
			resp.getWriter().flush();
			resp.getWriter().close();
		} else if ("saveCheckRolerNameInfo".equals(method)) {
			boolean flag = false;
			String rolerName = req.getParameter("rolerName");
			List<Rolers> list = rolerService.findAllRolerInfo();
			for (Rolers rolers : list) {
				//System.out.println(rolers.getRolerName().equals(rolerName));
				if (rolers.getRolerName().equals(rolerName)==true) {
					flag = true;
					break;
				} else {
					flag = false;
					break;
				}
			}
			resp.getWriter().print(flag);
			resp.getWriter().flush();
			resp.getWriter().close();
		}
	}
}
