/**
 * Administrator
 */
package com.demo.servlet;

import java.io.IOException;
import java.io.OutputStream;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.demo.entity.Dept;
import com.demo.entity.Employee;
import com.demo.service.DeptService;
import com.demo.service.DeptServiceImpI;
import com.demo.service.EmpService;
import com.demo.service.EmpServiceImpI;
import com.demo.utils.ExportDeptExcel;
import com.demo.utils.ExportEmployeeExcel;

/**
 * @author Administrator
 * 
 */
public class ExportExcelServlet extends HttpServlet {

	/**
	 * @author Administrator
	 * 
	 */
	private static final long serialVersionUID = -417558181480115937L;

	private DeptService deptService = new DeptServiceImpI();
	private EmpService empService=new EmpServiceImpI();

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		this.doPost(req, resp);

	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		req.setCharacterEncoding("utf-8");
		resp.setCharacterEncoding("utf-8");
		String method = req.getParameter("m");
		if ("exportDept".equals(method)) {
			resp.addHeader("Content-Disposition",
					"attachment;filename=Dept.xls");
			List<Dept> list=deptService.findAllDeptInfo();
			ExportDeptExcel<Dept> ex = new ExportDeptExcel<Dept>();
			String[] headers = { "部门编号", "部门名称", "创建时间", "修改时间", "部门描述", "部门备注" };
			try {
				OutputStream out=resp.getOutputStream();
				ex.exportExcel(headers,list, out);
				out.close();
			} catch (IOException e){
				e.printStackTrace();
			}		
			}else if ("exportEmp".equals(method)) {
				resp.addHeader("Content-Disposition", "attachment;filename=Employee.xls");
				List<Employee> list=empService.findAllInfo();
				ExportEmployeeExcel<Employee> ex=new ExportEmployeeExcel<Employee>();
				String[] headers={"员工编号","员工姓名","性别","出生日期","描述","备注","所在部门","学历","角色"};
				try {
					OutputStream out=resp.getOutputStream();
					ex.exportExcel(headers,list, out);
					out.close();
				} catch (IOException e){
					e.printStackTrace();
				}	
			}
	}

}
