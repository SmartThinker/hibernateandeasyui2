/**
 * 
 */
package com.demo.dao;

import java.util.List;

/**
 * @author Administrator
 * 
 */
public interface Dao {

	void saveObject(Object object);

	void updateObject(Object object);

	void deleteObject(Object object);

	Object getObject(String HQL);

	List<?> findWithPage(int page, int rows, String HQL);

	List<?> findAllInfo(String HQL);
}
