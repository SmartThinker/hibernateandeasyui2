/**
 * 
 */
package com.demo.dao;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Session;

import com.demo.utils.HibernateUtils;

/**
 * @author Administrator
 * 
 */
@SuppressWarnings("rawtypes")
public class DaoImpI implements Dao {

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.demo.dao.Dao#saveObject(java.lang.Object)
	 */
	@Override
	public void saveObject(Object object) {

		Session session = HibernateUtils.getSession();
		try {
			session.getTransaction().begin();
			session.save(object);
			session.getTransaction().commit();
		} catch (Exception e) {
			e.getStackTrace();
			session.getTransaction().rollback();
		} finally {
			HibernateUtils.getClosed(session);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.demo.dao.Dao#updateObject(java.lang.Object)
	 */
	@Override
	public void updateObject(Object object) {

		Session session = HibernateUtils.getSession();
		try {
			session.getTransaction().begin();
			session.update(object);
			session.getTransaction().commit();
		} catch (Exception e) {
			e.getStackTrace();
			session.getTransaction().rollback();
		} finally {
			HibernateUtils.getClosed(session);
		}

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.demo.dao.Dao#deleteObject(java.lang.Object)
	 */
	@Override
	public void deleteObject(Object object) {

		Session session = HibernateUtils.getSession();
		try {
			session.getTransaction().begin();
			session.delete(object);
			session.getTransaction().commit();
		} catch (Exception e) {
			e.getStackTrace();
			session.getTransaction().rollback();
		} finally {
			HibernateUtils.getClosed(session);
		}

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.demo.dao.Dao#findWithPage(int, int, java.lang.String)
	 */
	@Override
	public List<?> findWithPage(int page, int rows, String HQL) {
		List<?> list = new ArrayList();
		Session session = HibernateUtils.getSession();

		try {
			list=session.createQuery(HQL).setFirstResult((page - 1) * rows)
					.setMaxResults(rows).list();
		} catch (Exception e) {
			e.getStackTrace();
		} finally {
			HibernateUtils.getClosed(session);
		}
		return list;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.demo.dao.Dao#findAllInfo(java.lang.String)
	 */
	
	@Override
	public List<?> findAllInfo(String HQL) {
		Session session = HibernateUtils.getSession();
		List<?> list = new ArrayList();
		try {
			list=session.createQuery(HQL).list();
		} catch (Exception e) {
			e.getStackTrace();
		} finally {
			HibernateUtils.getClosed(session);
		}
		return list;
	}

	@Override
	public Object getObject(String HQL) {
		Session session = HibernateUtils.getSession();
		Object object = null;
		try {
			object=session.createQuery(HQL).uniqueResult();
		} catch (Exception e) {

		}
		return object;
	}

}
