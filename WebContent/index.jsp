<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>index.jsp</title>
<link rel="stylesheet" type="text/css"
	href="easyui/themes/default/easyui.css">
<link rel="stylesheet" type="text/css" href="easyui/themes/icon.css">
<script type="text/javascript" src="easyui/jquery.min.js"></script>
<script type="text/javascript" src="easyui/jquery.easyui.min.js"></script>
<script type="text/javascript" src="easyui/locale/easyui-lang-zh_CN.js"></script>
<script type="text/javascript" src="js/index.js"></script>
<script type="text/javascript" src="js/login.js"></script>
<style type="text/css">
#showLoginDiv{
position:relative;
	top: 20px;
}
</style>
</head>
<body class="easyui-layout">
	<!-- 登陆页面开始 -->
	<div id="loginDialog" class="easyui-dialog">
		<form action="" method="post" id="loginDialogForm"
			name="loginDialogForm">
			<div id="showLoginDiv">
			<table align="center">
				<tr align="right">
					<td><label>用户名：</label></td>
					<td><input class="easyui-validatebox" type="text"
						name="userName" id="userName" data-options="required:true"
						maxlength="19" /></td>
				</tr>

				<tr align="right">
					<td><label>密    码：</label></td>
					<td><input class="easyui-validatebox" type="password"
						name="userPassword" id="userPassword" data-options="required:true"
						maxlength="19" /></td>
				</tr>
			</table></div>
		</form>
	</div>
	<!-- 登陆页面结束 -->
	<div data-options="region:'north'" style="height: 85px;">
		<img alt="" src="images/logo.jpg">
	</div>

	<div data-options="region:'south'" style="height: 20px;" align="center">
	<span>西安软件服务外包学院</span>
	</div>

	<div data-options="region:'west',title:'导航菜单',split:true"
		style="width: 180px;">
		<div id="tree"></div>
	</div>

	<div data-options="region:'center',title:'数据展示'"
		style="background: #eee;">

		<div class="easyui-tabs" id="dataTabs"
			style="width: 500px; height: 300px;" fit="true">
			<div title="首页" align="center">
				<h1>欢迎访问西安软件服务外包学院</h1>
			</div>

		</div>
	</div>
</body>
</html>