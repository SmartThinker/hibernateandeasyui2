<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>dept</title>
<link rel="stylesheet" type="text/css"
	href="../easyui/themes/default/easyui.css">
<link rel="stylesheet" type="text/css" href="../easyui/themes/icon.css">
<script type="text/javascript" src="../easyui/jquery.min.js"></script>
<script type="text/javascript" src="../easyui/jquery.easyui.min.js"></script>
<script type="text/javascript"
	src="../easyui/locale/easyui-lang-zh_CN.js"></script>

<script type="text/javascript" src="../js/dept.js"></script>
</head>
<body>

	<!-- 显示列表数据 -->
	<table id="showDeptList"></table>
	<!-- 添加操作 -->
	<div id="addDeptDialog" class="easyui-dialog" title="添加部门信息"
		style="width: 300px; height: 400px;"
		data-options="resizable:true,modal:true,closed:true">
		<form id="addDeptForm" action="" method="post">
			<table align="center">
				<tr>
					<td><label>部门名称</label><input name="deptName" type="text"
						class="easyui-validatebox" required="true" maxlength="20"></td>
				</tr>
				<tr>
					<td><label>创建时间</label><input name="createTime" type="text"
						class="easyui-datebox" required="true" editable="false"></td>
				</tr>
				<tr>
					<td><label>修改时间</label><input name="modifyTime" type="text"
						class="easyui-datebox" required="true" editable="false"></td>
				</tr>
				<tr>
					<td><label>部门描述</label><input name="deptDesc" type="text"
						class="easyui-validatebox"></td>
				</tr>
				<tr align="right">
					<td><label>备注</label><input name="deptRemark" type="text"
						class="easyui-validatebox"></td>
				</tr>
			</table>
		</form>
	</div>
	<!-- 添加操作结束 -->
	<!-- 修改操作开始 -->
	<div id="updateDeptDialog" class="easyui-dialog" title="修改部门信息"
		style="width: 300px; height: 400px;"
		data-options="resizable:true,modal:true,closed:true">
		<form id="updateDeptForm" action="" method="post">
			<table align="center">
				<tr>
					<td><label>部门名称</label><input name="deptName" type="text"
						class="easyui-validatebox" required="true" maxlength="20"></td>
				</tr>
				<tr>
					<td><label>创建时间</label><input name="createTime" type="text"
						class="easyui-datebox" required="true" editable="false"></td>
				</tr>
				<tr>
					<td><label>修改时间</label><input name="modifyTime" type="text"
						class="easyui-datebox" required="true" editable="false"></td>
				</tr>
				<tr>
					<td><label>部门描述</label><input name="deptDesc" type="text"
						class="easyui-validatebox"></td>
				</tr>
				<tr align="right">
					<td><label>备注</label><input name="empRemark" type="text"
						class="easyui-validatebox"></td>
				</tr>
			</table>
		</form>
	</div>
	<!-- 修改信息结束 -->
</body>
</html>