<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>limits</title>
<link rel="stylesheet" type="text/css"
	href="../easyui/themes/default/easyui.css">
<link rel="stylesheet" type="text/css" href="../easyui/themes/icon.css">
<script type="text/javascript" src="../easyui/jquery.min.js"></script>
<script type="text/javascript" src="../easyui/jquery.easyui.min.js"></script>
<script type="text/javascript" src="../easyui/locale/easyui-lang-zh_CN.js"></script>

<script type="text/javascript" src="../js/limits.js"></script>
</head>
<body>
<table id="showLimitsList"></table> 
<!-- 添加操作 -->
	<div id="addLimitsDialog" class="easyui-dialog" title="添加权限信息"
		style="width: 300px; height: 400px;"
		data-options="resizable:true,modal:true,closed:true">
		<form id="addLimitsForm" action="" method="post">
				<table align="center">
				<tr align="right">
					<td><label>权限名称</label><input name="limitName" type="text"
						class="easyui-validatebox" required="true" maxlength="20"></td>
				</tr>
				<tr align="right">
					<td><label>权限描述</label><input name="limitDesc" type="text"
						class="easyui-validatebox" required="true" editable="false"></td>
				</tr>
				<tr align="right">
					<td><label>备注</label><input name="limitRemark" type="text"
						class="easyui-validatebox" required="true" editable="false"></td>
				</tr>
			</table>
		</form>
	</div>
	<!-- 添加操作结束 -->
	<!-- 修改操作开始 -->
	<div id="updateLimitsDialog" class="easyui-dialog" title="修改权限信息"
		style="width: 300px; height: 400px;"
		data-options="resizable:true,modal:true,closed:true">
		<form id="updateLimitsForm" action="" method="post">
				<table align="center">
				<tr align="right">
					<td><label>权限名称</label><input name="limitName" type="text"
						class="easyui-validatebox" required="true" maxlength="20"></td>
				</tr>
				<tr align="right">
					<td><label>权限描述</label><input name="limitDesc" type="text"
						class="easyui-validatebox" required="true" editable="false"></td>
				</tr>
				<tr align="right">
					<td><label>备注</label><input name="limitRemark" type="text"
						class="easyui-validatebox" required="true" editable="false"></td>
				</tr>
			</table>
		</form>
	</div>
	<!-- 修改信息结束 -->
</body>
</html>