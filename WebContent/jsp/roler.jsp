<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>rolerList</title>
<link rel="stylesheet" type="text/css"
	href="../easyui/themes/default/easyui.css">
<link rel="stylesheet" type="text/css" href="../easyui/themes/icon.css">
<script type="text/javascript" src="../easyui/jquery.min.js"></script>
<script type="text/javascript" src="../easyui/jquery.easyui.min.js"></script>
<script type="text/javascript"
	src="../easyui/locale/easyui-lang-zh_CN.js"></script>

<script type="text/javascript" src="../js/roler.js"></script>
</head>
<body>
	<table id="showRolerList"></table>
	<!-- 添加操作 -->
	<div id="addRolerDialog" class="easyui-dialog" title="添加角色信息"
		style="width: 300px; height: 400px;"
		data-options="resizable:true,modal:true,closed:true">
		<form id="addRolerForm" action="" method="post">
			<table align="center">
				<tr align="right">
					<td><label>角色名称</label><input name="rolerName" type="text"
						class="easyui-validatebox" required="true" maxlength="20"></td>
				</tr>
				<tr align="right">
					<td><label>角色描述</label><input name="rolerDesc" type="text"
						class="easyui-validatebox" required="true" editable="false"></td>
				</tr>
				<tr align="right">
					<td><label>备注</label><input name="rolerRemark" type="text"
						class="easyui-validatebox" required="true" editable="false"></td>
				</tr>
				<tr align="right">
					<td><label>权限</label><input name="limitId" 
						class="easyui-combobox" required="true"
						data-options="valueField:'limitId',textField:'limitName',url:'../limit?m=findAllInfo'"></td>
				</tr>
			</table>
		</form>
	</div>
	<!-- 添加操作结束 -->
	<!-- 修改操作开始 -->
	<div id="updateRolerDialog" class="easyui-dialog" title="修改部门信息"
		style="width: 300px; height: 400px;"
		data-options="resizable:true,modal:true,closed:true">
		<form id="updateRolerForm" action="" method="post">
			<table align="center">
				<tr align="right">
					<td><label>角色名称</label><input name="rolerName" type="text"
						class="easyui-validatebox" required="true" maxlength="20"></td>
				</tr>
				<tr align="right">
					<td><label>角色描述</label><input name="rolerDesc" type="text"
						class="easyui-validatebox" required="true" editable="false"></td>
				</tr>
				<tr align="right">
					<td><label>备注</label><input name="rolerRemark" type="text"
						class="easyui-validatebox" required="true" editable="false"></td>
				</tr>
				<tr align="right">
					<td><label>权限</label><input name="limitId"
						class="easyui-combobox" required="true" id="limitAAasd"
						data-options="valueField:'limitId',textField:'limitName',url:'../limit?m=findAllInfo'"></td>
				</tr>
			</table>
		</form>
	</div>
	<!-- 修改信息结束 -->
</body>
</html>