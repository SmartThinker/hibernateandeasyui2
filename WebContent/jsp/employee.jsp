<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>employee.jsp</title>
<link rel="stylesheet" type="text/css"
	href="../easyui/themes/default/easyui.css">
<link rel="stylesheet" type="text/css" href="../easyui/themes/icon.css">
<script type="text/javascript" src="../easyui/jquery.min.js"></script>
<script type="text/javascript" src="../easyui/jquery.easyui.min.js"></script>
<script type="text/javascript"
	src="../easyui/locale/easyui-lang-zh_CN.js"></script>

<script type="text/javascript" src="../js/employee.js"></script>

</head>
<body>
	<!-- 查询数据列表开始 -->
	<table id="showEmpList"></table>
	<!-- 查询数据列表结束 -->
	<!-- 添加员工开始 -->
		<div id="addEmpDialog" class="easyui-dialog" title="添加部门信息"
			style="width: 300px; height: 300px;"
			data-options="resizable:true,modal:true,closed:true">
			<form id="addEmpForm" action="" method="post">
				<table align="center">
					<tr align="right">
						<td><label>姓名</label><input name="empName" type="text"
							class="easyui-validatebox" required="true" maxlength="20"></td>
					</tr>
					<tr align="right">
						<td><label>性别</label><select id="sex" class="easyui-combobox"
							name="sex" style="width: 100px;">
								<option value="0">男</option>
								<option value="1">女</option>
						</select></td>
					</tr>
					<tr align="right">
						<td><label>出生日期</label><input name="birthDay" type="text"
							class="easyui-datebox" required="true" editable="false"></td>
					</tr>
					<tr align="right">
						<td><label>员工简介</label><input name="empDesc" type="text"
							class="easyui-validatebox"></td>

					</tr>
					<tr align="right">
						<td><label>备注</label><input name="empRemark" type="text"
							class="easyui-validatebox"></td>
					</tr>
					<tr align="right">
						<td><label>所在部门</label><input name="deptId"
							class="easyui-combobox"
							data-options="valueField:'deptId',textField:'deptName',url:'../heHe?m=findAllInfo'"></td>
					</tr>
					<tr align="right">
						<td><label>学历</label><input name="educationId"
							class="easyui-combobox"
							data-options="valueField:'educationId',textField:'eduName',url:'../edu?m=findAllEduInfo'"></td>
					</tr>
					<tr align="right">
						<td><label>角色</label><input name="rolerId"
							class="easyui-combobox"
							data-options="valueField:'rolerId',textField:'rolerName',url:'../roler?m=findAllRolerInfo'"></td>
					</tr>
				</table>
			</form>
		</div>
		<!-- 添加员工结束 -->
		<!-- 修改员工开始-->
			<div id="updateEmpDialog" class="easyui-dialog" title="修改部门信息"
				style="width: 300px; height: 300px;"
				data-options="resizable:true,modal:true,closed:true">
				<form id="updateEmpForm" action="" method="post">
					<table align="center">
					<tr align="right">
						<td><label>姓名</label><input name="empName" type="text"
							class="easyui-validatebox" required="true" maxlength="20"></td>
					</tr>
					<tr align="right">
						<td><label>性别</label><select id="sex" class="easyui-combobox"
							name="sex" >
								<option value="0">男</option>
								<option value="1">女</option>
						</select></td>
					</tr>
					<tr align="right">
						<td><label>出生日期</label><input name="birthDay" type="text"
							class="easyui-datebox" required="true" editable="false"></td>
					</tr>
					<tr align="right">
						<td><label>员工简介</label><input name="empDesc" type="text"
							class="easyui-validatebox"></td>
					</tr>
					<tr align="right">
						<td><label>备注</label><input name="empRemark" type="text"
							class="easyui-validatebox"></td>
					</tr>
					<tr align="right">
						<td><label>所在部门</label><input name="deptId" id="deptAA"
							class="easyui-combobox" 
							data-options="valueField:'deptId',textField:'deptName',url:'../heHe?m=findAllInfo'"></td>
					</tr>
					<tr align="right">
						<td><label>学历</label><input name="educationId" id="eduAA"
							class="easyui-combobox"
							data-options="valueField:'educationId',textField:'eduName',url:'../edu?m=findAllEduInfo'"></td>
					</tr>
					<tr align="right">
						<td><label>角色</label><input name="rolerId" id="rolerAA"
							class="easyui-combobox"
							data-options="valueField:'rolerId',textField:'rolerName',url:'../roler?m=findAllRolerInfo'"></td>
					</tr>
				</table>
				</form>
			</div>
			<!-- 修改员工结束 -->

</body>
</html>