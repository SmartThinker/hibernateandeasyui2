if("${empty userType}"=="true"){
 	window.location="../index.jsp";
}
$(function(){
	$("#loginDialog").dialog({
		width:350,
		height:250,
		title:"登录管理",
		modal:true,
		closable:false,
		buttons:[{
			text:"登录",
			handler:function(){
				
				if ($("#loginDialogForm").form('validate')) {
					$.post("login?m=checkUserAccess&"+$("#loginDialogForm").serialize(),function(data){
						if(data=='0'){
							$.messager.alert('提示','用户不存在，请重输','info');
							return;
						}
						if (data=='2') {
						$("#loginDialog").dialog('close');
						$.messager.show({
							title:'提示',
							msg:'恭喜 '+" 登录成功",
							timeout:5000,
							showType:'slide'
						});
					}if(data=='3'){
						$.messager.alert('提示','密码不正确，请重输','info');
						return;
					}
					});
				}
			}
			
		},{
			text:"重置",
			handler:function(){
				$("#loginDialogForm").form('clear');
				return;
			}
		}]
		
	});
	
});