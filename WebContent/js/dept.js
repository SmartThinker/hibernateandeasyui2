if("${empty userType}"=="true"){
 	window.location="../index.jsp";
}
$(function() {
	// 展示列表
	$("#showDeptList")
			.datagrid(
					{

						fit : true,
						fitColumns : true,
						striped : true,
						nowrap : true,
						singleSelect:true,
						loadMsg : "请稍后，部门列表正在加载中...",
						pagination : true,
						singleSelect:true,
						url : '../heHe?m=findWithPage',
						columns : [ [ {
							field : 'deptId',
							title : '部门编号',
							width : 100,
						}, {
							field : 'deptName',
							title : '部门名称',
							width : 100
						}, {
							field : 'createTime',
							title : '创建时间',
							width : 100
						}, {
							field : 'modifyTime',
							title : '修改时间',
							width : 100
						}, {
							field : 'deptDesc',
							title : '部门描述',
							width : 100
						}, {
							field : 'empRemark',
							title : '备注',
							width : 100
						} ] ],
						toolbar : [
								{
									iconCls : 'icon-dept_add',
									text : "添加",
									handler : function() {
										//添加操作//
										$("#addDeptDialog").dialog(
														{
															closed : false,
															buttons : [
																	{
																		text : "保存",
																		handler : function() {
																			//向后台发送查询rolerName数据
																			$.post("../heHe?m=saveCheckDeptNameInfo&"+$("#addDeptForm").serialize(),function(data){
																				//alert(data);
																				if (data=='true') {
																					$.messager.show({
																						title:'提示',
																						msg:'对不起已存在',
																						timeout:5000,
																						showType:'slide'
																					});
																					return;
																				}if(data=='false'){
																					//alert(data+'asdasdas');
																			if ($(
																					"#addDeptForm")
																					.form(
																							'validate')) {
																				$
																						.post("../heHe?m=saveDeptInfo&"
																								+ $(
																										"#addDeptForm")
																										.serialize());
																				$(
																						"#addDeptDialog")
																						.dialog(
																								{
																									closed : true
																								});
																				$(
																						"#addDeptForm")
																						.form(
																								'clear');
																				$(
																						"#showDeptList")
																						.datagrid(
																								'reload');
																			}
																		}
																			});
																			}
																	},
																	{
																		text : "重置",
																		handler : function() {
																			$(
																					"#addDeptForm")
																					.form(
																							'clear');
																		}
																	} ]

														});
									}
								},
								'-',
								{
									iconCls : 'icon-dept_edit',
									text : "编辑",
									handler : function() {
										if ($("#showDeptList").datagrid(
												'getSelected') == null) {
											alert('请选择要修改的数据！');
											return;
										} else {
											$("#updateDeptForm")
													.form(
															'load',
															"../heHe?m=findByIdIfo&f_id="
																	+ $(
																			"#showDeptList")
																			.datagrid(
																					'getSelected').deptId);
											// 修改操作
											$("#updateDeptDialog")
													.dialog(
															{
																closed : false,
																buttons : [
																		{
																			text : "保存",
																			handler : function() {
																				if ($(
																						"#updateDeptForm")
																						.form(
																								'validate')) {
																					$
																							.post("../heHe?m=updateDeptInfo&"
																									+ $(
																											"#updateDeptForm")
																											.serialize());
																					$(
																							"#updateDeptDialog")
																							.dialog(
																									{
																										closed : true
																									});
																					$(
																							"#updateDeptForm")
																							.form(
																									'clear');
																					$(
																							"#showDeptList")
																							.datagrid(
																									'reload');
																				}
																			}
																		},
																		{
																			text : "重置",
																			handler : function() {
																				$("#updateDeptForm").form('clear');
																			}
																		} ]

															});
										}
									}
								},
								'-',
								{
									iconCls : 'icon-dept_remove',
									text : "删除",
									handler : function() {
										if ($("#showDeptList").datagrid(
												'getSelected') == null) {
											alert('您还没有选择数据！');
											return;
										} else {
											$.post("../heHe?m=deleteDeptInfo&d_id="
															+ $("#showDeptList").datagrid('getSelected').deptId);
											$("#showDeptlist").datagrid('load');
										}
									}
								}, '-', {
									iconCls : 'icon-reload',
									text : "刷新",
									handler : function() {
										$("#showDeptList").datagrid('reload');
										return;
									}
								}, '-', {
									iconCls : '',
									text : "导出Excel",
									handler : function() {
										window.open("../ExportExcelServlet?m=exportDept");
									}
								}]

					});

});