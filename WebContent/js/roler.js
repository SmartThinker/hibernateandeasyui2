if("${empty userType}"=="true"){
 	window.location="../index.jsp";
}
$(function() {
	// 展示列表
	$("#showRolerList").datagrid({

		fit : true,
		fitColumns : true,
		striped:true,
		nowrap:true,singleSelect:true,
		url : '../roler?m=findWithPage',
		loadMsg:"请稍后，角色列表正在加载中...",
		pagination:true,
		columns : [ [ {
			field : 'rolerId',
			title : '角色编号',
			width : 100
		}, {
			field : 'rolerName',
			title : '角色名称',
			width : 100
		}, {
			field : 'rolerDesc',
			title : '角色描述',
			width : 100
		}, {
			field : 'rolerRemark',
			title : '备注',
			width : 100
		} , {
			field : 'limits',
			title : '权限',
			width : 100,
			formatter: function(value,row,index){
				if (row.limits){
					return row.limits.limitName;
				} else {
					return value;
				}
			}

		}] ],
		toolbar : [
					{
						iconCls : 'icon-roler_add',
						text : "添加",
						handler : function() {
							//添加操作//
							$("#addRolerDialog")
									.show()
									.dialog(
											{
												closed : false,
												buttons : [
														{
															text : "保存",
															handler : function() {
																//向后台发送查询rolerName数据
																$.post("../roler?m=saveCheckRolerNameInfo&"+$("#addRolerForm").serialize(),function(data){
																	//alert(data);
																	if (data=='true') {
																		$.messager.show({
																			title:'提示',
																			msg:'对不起已存在',
																			timeout:5000,
																			showType:'slide'
																		});
																		return;
																	}if(data=='false'){
																		//
																		//alert(data+'asdasdas');
																		if ($(
																		"#addRolerForm")
																		.form(
																				'validate')) {
																	$
																			.post("../roler?m=saveRolerInfo&"
																					+ $(
																							"#addRolerForm")
																							.serialize());
																	$(
																			"#addRolerDialog")
																			.dialog(
																					{
																						closed : true
																					});
																	$(
																			"#addRolerForm")
																			.form(
																					'clear');
																	$(
																			"#showRolerList")
																			.datagrid(
																					'reload');
																}
																		//
																		
																	}
																	
																	});
																//
														
																
															}
														},
														{
															text : "重置",
															handler : function() {
																$(
																		"#addRolerForm")
																		.form(
																				'clear');
															}
														} ]

											});
						}
					},
					'-',
					{
						iconCls : 'icon-roler_edit',
						text : "编辑",
						handler : function() {
							if ($("#showRolerList").datagrid(
									'getSelected') == null) {
								alert('请选择要修改的数据！');
								return;
							} else {
								$("#updateRolerForm")
										.form(
												'load',
												"../roler?m=findByIdIfo&f_id="
														+ $(
																"#showRolerList")
																.datagrid(
																		'getSelected').rolerId);
								$('#limitAAasd').combobox(
										'select',$("#showRolerList").datagrid('getSelected').limits.limitName
								);
								// 修改操作
								$("#updateRolerDialog")
										.dialog(
												{
													closed : false,
													buttons : [
															{
																text : "保存",
																handler : function() {
																	if ($(
																			"#updateRolerForm")
																			.form(
																					'validate')) {
																		$
																				.post("../roler?m=updateRolerInfo&"
																						+ $(
																								"#updateRolerForm")
																								.serialize());
																		$(
																				"#updateRolerDialog")
																				.dialog(
																						{
																							closed : true
																						});
																		$(
																				"#updateRolerForm")
																				.form(
																						'clear');
																		$(
																				"#showRolerList")
																				.datagrid(
																						'reload');
																	}
																}
															},
															{
																text : "重置",
																handler : function() {
																	$("#updateRolerForm").form('clear');
																}
															} ]
												});
							}
						}
					},
					'-',
					{
						iconCls : 'icon-roler_remove',
						text : "删除",
						handler : function() {
							if ($("#showRolerList").datagrid(
									'getSelected') == null) {
								alert('您还没有选择数据！');
								return;
							} else {
								$.post("../roler?m=deleteRolerInfo&d_id="
												+ $("#showRolerList").datagrid('getSelected').rolerId);
								$("#showRolerlist").datagrid('load');
							}
						}
					}, '-', {
						iconCls : 'icon-reload',
						text : "刷新",
						handler : function() {
							$("#showRolerList").datagrid('reload');
							return;
						}
					}]

		});

});