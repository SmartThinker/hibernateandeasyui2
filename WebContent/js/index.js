if("${empty userType}"=="true"){
 	window.location="../index.jsp";
}
$(function() {
	var treeData = [ {
		text : '系统管理',
		iconCls : 'icon-tree_first',
		children : [ {
			text : '部门管理',
			iconCls : 'icon-dept',
			attributes : {
				url : 'jsp/dept.jsp'
			}

		}, {
			text : '员工管理',
			iconCls : 'icon-emp',
			attributes : {
				url : 'jsp/employee.jsp'
			}

		}, {
			text : '角色管理',
			iconCls : 'icon-roler',
			attributes : {
				url : 'jsp/roler.jsp'
			}

		}, {
			text : '权限管理',
			iconCls : 'icon-limit',
			attributes : {
				url : 'jsp/limits.jsp'
			}

		} ]
	} ];
	$("#tree").tree({
		data : treeData,
		animate:true,
		lines:true,
		onClick : function(node) {
			if (node.attributes) {
				getTab(node.text, node.iconCls,node.attributes.url);
			}
		}
	});

	function getTab(text, iconCls, url) {
		if ($("#dataTabs").tabs('exists', text)) {
			$("#dataTabs").tabs('select', text);
		} else {
			var content = "<iframe frameborder='0' scrolling='auto' style='width:100%;height:100%' src="
					+ url + "></iframe>";
			$("#dataTabs").tabs('add', {
				title : text,
				content : content,
				iconCls : iconCls,
				closable : true
			});
		}
	}

});