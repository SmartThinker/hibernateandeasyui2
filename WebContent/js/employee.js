if("${empty userType}"=="true"){
 	window.location="../index.jsp";
}
$(function(){
	$("#showEmpList").datagrid({
		fit : true,
		fitColumns : true,
		striped:true,
		nowrap:true,
		singleSelect:true,
		url:'../emp?m=findWithPage',
		loadMsg:"请稍后，员工列表正在加载中...",
		pagination:true,
		columns : [ [ {
			field : 'empId',
			title : '员工编号',
			width : 100
		}, {
			field : 'empName',
			title : '姓名',
			width : 100
		}, {
			field : 'sex',
			title : '性别',
			width : 100
		}, {
			field : 'birthDay',
			title : '出生日期',
			width : 100
		}, {
			field : 'empDesc',
			title : '员工简介',
			width : 100
		}, {
			field : 'empRemark',
			title : '备注',
			width : 100
		} , {
			field : 'dept',
			title : '所在部门',
			width : 100,
			formatter: function(value,row,index){
				if (row.dept){
					return row.dept.deptName;
				} 
			}

		}, {
			field : 'education',
			title : '学历',
			width : 100,
			formatter: function(value,row,index){
				if (row.education){
					return row.education.eduName;
				} 
			}
		}, {
			field : 'rolers',
			title : '角色',
			width : 100,
			formatter: function(value,row,index){
				if (row.rolers){
					return row.rolers.rolerName;
				}
			}
		}] ],
		toolbar:[{
			text:'增加',
			iconCls:'icon-emp_add',
			handler:function(){
				$("#addEmpDialog").show().dialog({
					closed:false,
					width:350,
					height:450,
					buttons : [
								{
									text : "保存",
									handler : function() {
										//向后台发送查询rolerName数据
										$.post("../emp?m=saveCheckEmpNameInfo&"+$("#addEmpForm").serialize(),function(data){
											//alert(data);
											if (data=='true') {
												$.messager.show({
													title:'提示',
													msg:'对不起已存在',
													timeout:5000,
													showType:'slide'
												});
												return;
											}if(data=='false'){
										if ($(
												"#addEmpForm")
												.form(
														'validate')) {
											$
													.post("../emp?m=saveEmpInfo&"
															+ $(
																	"#addEmpForm")
																	.serialize());
											$(
													"#addEmpDialog")
													.dialog(
															{
																closed : true
															});
											$(
													"#addEmpForm")
													.form(
															'clear');
											$(
													"#showEmpList")
													.datagrid(
															'reload');
										}
									}
										});
									}
								},
								{
									text : "重置",
									handler : function() {
										$(
												"#addEmpForm")
												.form(
														'clear');
									}
								} ]
				});
			}
		},"-",{
			text:'修改',
			iconCls:"icon-emp_edit",
			handler:function(){
				if ($("#showEmpList").datagrid(
				'getSelected') == null) {
			alert('请选择要修改的数据！');
			return;
		} else {
			$("#updateEmpForm")
					.form(
							'load',
							"../emp?m=findByIdIfo&f_id="
									+ $(
											"#showEmpList")
											.datagrid(
													'getSelected').empId);
			
			$("#deptAA").combobox(
					'select',$("#showEmpList").datagrid('getSelected').dept.deptName
			);
			$('#eduAA').combobox(
					'select',$("#showEmpList").datagrid('getSelected').education.eduName
			);
			$('#rolerAA').combobox('select',$("#showEmpList").datagrid('getSelected').rolers.rolerName);
			
			
			//操作
			$("#updateEmpDialog")
					.dialog(
							{
								closed : false,
								buttons : [
										{
											text : "保存",
											handler : function() {
												if ($(
														"#updateEmpForm")
														.form(
																'validate')) {
													$
															.post("../emp?m=updateEmpInfo&"
																	+ $(
																			"#updateEmpForm")
																			.serialize());
													$(
															"#updateEmpDialog")
															.dialog(
																	{
																		closed : true
																	});
													$(
															"#updateEmpForm")
															.form(
																	'clear');
													$(
															"#showEmpList")
															.datagrid(
																	'reload');
												}
											}
										},
										{
											text : "重置",
											handler : function() {
												$("#updateEmpForm").form('clear');
											}
										} ]

							});
		}
			}
		},"-",{
			text:'删除',
			iconCls:"icon-emp_remove",
			handler:function(){
				if ($("#showEmpList").datagrid(
				'getSelected') == null) {
			alert('您还没有选择数据！');
			return;
		} else {
			$.post("../emp?m=deleteEmpInfo&d_id="
							+ $("#showEmpList").datagrid('getSelected').empId);
			$("#showEmplist").datagrid('load');
		}
			}
		},"-",{
			text:'刷新',
			iconCls:"icon-reload",
			handler:function(){
				$("#showEmpList").datagrid('reload');
				return;
			}
		},"-",{
			text:'导出Excel',
			iconCls:"icon-print",
			handler:function(){
				window.open("../ExportExcelServlet?m=exportEmp");
			}
		}]
		
	});
	
	
});