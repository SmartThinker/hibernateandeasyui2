if("${empty userType}"=="true"){
 	window.location="../index.jsp";
}
$(function() {
	// 展示列表
	$("#showLimitsList").datagrid({

		fit : true,
		fitColumns : true,
		striped:true,
		nowrap:true,
		singleSelect:true,
		loadMsg:"请稍后，权限列表正在加载中...",
		url:'../limit?m=findWithPage',
		pagination:true,
		columns : [ [ {
			field : 'limitId',
			title : '权限编号',
			width : 100
		}, {
			field : 'limitName',
			title : '权限名称',
			width : 100
		}, {
			field : 'limitDesc',
			title : '权限描述',
			width : 100
		}, {
			field : 'limitRemark',
			title : '备注',
			width : 100
		} ] ],
		toolbar : [
					{
						iconCls : 'icon-limit_add',
						text : "添加",
						handler : function() {
							//添加操作//
							$("#addLimitsDialog")
									.show()
									.dialog(
											{
												closed : false,
												buttons : [
														{
															text : "保存",
															handler : function() {
																//向后台发送查询rolerName数据
																$.post("../limit?m=saveCheckEmpNameInfo&"+$("#addLimitsForm").serialize(),function(data){
																	//alert(data);
																	if (data=='true') {
																		$.messager.show({
																			title:'提示',
																			msg:'对不起已存在',
																			timeout:5000,
																			showType:'slide'
																		});
																		return;
																	}if(data=='false'){
																if ($(
																		"#addLimitsForm")
																		.form(
																				'validate')) {
																	$
																			.post("../limit?m=saveLimitInfo&"
																					+ $(
																							"#addLimitsForm")
																							.serialize());
																	$(
																			"#addLimitsDialog")
																			.dialog(
																					{
																						closed : true
																					});
																	$(
																			"#addLimitsForm")
																			.form(
																					'clear');
																	$(
																			"#showLimitsList")
																			.datagrid(
																					'reload');
																}
															}	});
															}
														},
														{
															text : "重置",
															handler : function() {
																$(
																		"#addLimitsForm")
																		.form(
																				'clear');
															}
														} ]

											});
						}
					},
					'-',
					{
						iconCls : 'icon-limit_edit',
						text : "编辑",
						handler : function() {
							if ($("#showLimitsList").datagrid(
									'getSelected') == null) {
								alert('请选择要修改的数据！');
								return;
							} else {
								$("#updateLimitsForm")
										.form(
												'load',
												"../limit?m=findByIdInfo&f_id="
														+ $(
																"#showLimitsList")
																.datagrid(
																		'getSelected').limitId);
								// 修改操作
								$("#updateLimitsDialog")
										.dialog(
												{
													closed : false,
													buttons : [
															{
																text : "保存",
																handler : function() {
																	if ($(
																			"#updateLimitsForm")
																			.form(
																					'validate')) {
																		$
																				.post("../limit?m=updateLimitInfo&"
																						+ $(
																								"#updateLimitsForm")
																								.serialize());
																		$(
																				"#updateLimitsDialog")
																				.dialog(
																						{
																							closed : true
																						});
																		$(
																				"#updateLimitsForm")
																				.form(
																						'clear');
																		$(
																				"#showLimitsList")
																				.datagrid(
																						'reload');
																	}
																}
															},
															{
																text : "重置",
																handler : function() {
																	$("#updateLimitsForm").form('clear');
																}
															} ]
												});
							}
						}
					},
					'-',
					{
						iconCls : 'icon-limit_remove',
						text : "删除",
						handler : function() {
							if ($("#showLimitsList").datagrid(
									'getSelected') == null) {
								alert('您还没有选择数据！');
								return;
							} else {
								$.post("../limit?m=deleteLimitInfo&d_id="
												+ $("#showLimitsList").datagrid('getSelected').limitId);
								$("#showLimitslist").datagrid('reload');
							}
						}
					}, '-', {
						iconCls : 'icon-reload',
						text : "刷新",
						handler : function() {
							$("#showLimitsList").datagrid('load');
							return;
						}
					}]

		});
});